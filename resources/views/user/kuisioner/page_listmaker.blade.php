@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('breadcrumb')
	<h2><b>List Maker Kuesioner</b></h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">Laravel Pro</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>List Maker</strong>
		</li>
	</ol>
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<b>List Maker Kuesioner</b>
				</div>
				<div class="panel-body">
					<div class="project-list">
						<table class="table table-hover">
							<tbody>
								@foreach ($dtKuisioner as $index => $dk)
									<tr>
										<td class="project-status">
											<span class="label label-primary">Active</span>
										</td>
										<td class="project-title">
											<a href="project_detail.html">{{$dk->name}}</a>
											<br/>
											<small>{{$dk->email}}</small>
										</td>
										<td class="project-completion">
											<small>Jumlah Kuesioner : {{$dk->jum_kuis}}</small>
											<div class="progress progress-mini">
												<div style="width: {{ round(($dtJawabUser[$index]->jum_jawab * 100) / $dk->jum_kuis) }}%;" class="progress-bar"></div>
											</div>
										</td>
										<td class="project-people">
											@if(Auth::user()->avatar))
												<a href="#"><img alt="image" class="rounded-circle" src="{{asset('public/storage/'.Auth::user()->avatar)}}"></a>
											@else
												<a href="#"><img alt="image" class="rounded-circle" src="{{asset('public/template_inspinia/img/thumb.png')}}"></a>
											@endif
											@if($dk->avatar)
												<a href="#"><img alt="image" class="rounded-circle" src="{{asset('public/storage/'.$dk->avatar)}}"></a>
											@else
												<a href="#"><img alt="image" class="rounded-circle" src="{{asset('public/template_inspinia/img/17004.png')}}"></a>
											@endif
										</td>
										<td class="project-actions">
											<a href="#" class="btn btn-outline btn-info btn-sm"><i class="fa fa-folder"></i> Detail Maker </a>
											@if($dk->jum_kuis == $dtJawabUser[$index]->jum_jawab)
												<a href="{{ url('/kuisioner/poin', $dk->id) }}" class="btn btn-outline btn-success btn-sm"><i class="fa fa-bar-chart-o"></i> Show Poin </a>
											@elseif($dk->jum_kuis < $dtJawabUser[$index]->jum_jawab)
												<a href="#" class="btn btn-danger btn-sm"><i class="fa fa-exclamation"></i> Error </a>
											@else
												<a href="{{ url('/kuisioner/start', $dk->id) }}" class="btn btn-outline btn-primary btn-sm"><i class="fa fa-pencil"></i> Start Work </a>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

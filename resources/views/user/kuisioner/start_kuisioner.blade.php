@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('css_script')
<link href="{{asset('public/template_inspinia/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
<link href="{{asset('public/template_inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">
@endsection

@section('breadcrumb')
	<h2><b>Start Working on ..</b></h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">Laravel Pro</a>
		</li>
		<li class="breadcrumb-item">
			<a href="{{url('/listmaker')}}">List Maker</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>Start Working</strong>
		</li>
	</ol>
@endsection

@section('content')
	@if(session('status'))
		<div class="alert alert-success">
			{{session('status')}}
		</div>
	@endif
	<div class="row">
        <div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title bg-info">
					<h5>## Start Working on [The Questionnaire {{ $pembuat->name }}] ##</h5>
				</div>
				<div class="ibox-content">
					<h2> Form Data Kuesioner<br></h2>
					<p>
						<strong>Tata Cara Pengerjaan :</strong>
						Harap tekan tombol save untuk menyimpan. Total poin akan tampil saat sudah menyelesaikan semua perntanyaan
					</p>
					<form action="{{url('/simpan_kuesioner')}}" method="POST" enctype="multipart/form-data" style="padding-top:20px;">
						@csrf
						@foreach ($soal as $index => $s)
							<input type="hidden" name="identity{{$index}}" value="{{ $s->id }}">
							<div class="row">
								<div class="col-md-12"><h2><b> {{($index + $soal->firstItem())}}. {{$s->soal}} </b></h2></div>
							</div>
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-2">
									<div class="i-checks"><b><label> <input type="radio" value="YA" name="jawab{{$index}}" {{ ($s->jawaban) ? (($s->jawaban == 'YA') ? 'checked' : '') : 'checked' }} > <i></i> Ya </label></b></div>
								</div>
								<div class="col-md-2">
									<div class="i-checks"><b><label> <input type="radio" value="TIDAK" name="jawab{{$index}}" {{ ($s->jawaban) ? (($s->jawaban == 'TIDAK') ? 'checked' : '') : '' }} > <i></i> Tidak </label></b></div>
								</div>
								<div class="col-md-2">
									<div class="i-checks"><b><label> <input type="radio" value="KEDUANYA" name="jawab{{$index}}" {{ ($s->jawaban) ? (($s->jawaban == 'KEDUANYA') ? 'checked' : '') : '' }} > <i></i> Bisa YA bisa Tidak </label></b></div>
								</div>
								<div class="col-md-5 text-center">
									@if($s->updated_at)
										<h3 style="color:yellow"><b>Diubah pada tanggal {{$s->updated_at}}</b></h2>
									@elseif($s->created_at)
										<h3 style="color:blue"><b>Dijawab pada tanggal {{$s->created_at}}</b></h2>
									@else
										<h3 style="color:red"><b>Belum Dijawab</b></h2>
									@endif
								</div>
							</div>
							<br>
						@endforeach
						
						<input type="hidden" name="jumlah" value="{{$soal->perPage()}}">
						<div class="hr-line-dashed"></div>
						<div class="form-group row">
							<div class="col-sm-5 col-sm-offset-2">
								<a href="{{url('/listmaker')}}" class="btn btn-warning btn-sm">{{ (count($cek) == $soal->total()) ? 'Finish' : '<< Back List Maker' }}</a>
								<button class="btn btn-primary btn-sm" name="btn_submit" type="submit">Save changes</button>
							</div>
							<div class="col-sm-6">
								{{$soal->links()}}
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
							@if(count($cek) == $soal->total())
								<b> Catata : Anda Sudah Menjawab Semua Pertanyaan..!! </b>
							@endif
							</div>
						</div>
					</form>
				</div>
				<div class="ibox-footer">
					<span class="float-right"> The righ side of the footer </span>
					This is simple footer example
				</div>
			</div>
        </div>
    </div>
@endsection

@section('js_script')
<script src="{{asset('public/template_inspinia/js/plugins/iCheck/icheck.min.js')}}"></script>
@endsection

@section('script_tambahan')
<script>
	$(document).ready(function () {
		$('.i-checks').iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
		});
	});
</script>
@endsection
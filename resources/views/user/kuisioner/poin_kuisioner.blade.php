@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('breadcrumb')
	<h2><b>Poin of Questionnaire</b></h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">Laravel Pro</a>
		</li>
		<li class="breadcrumb-item">
			<a href="{{url('/listmaker')}}">List Maker</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>Show Poin</strong>
		</li>
	</ol>
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title bg-info">
					<h5>## Poin of Questionnaire [The Questionnaire {{ $pembuat->name }}] ##</h5>
				</div>
				<div class="ibox-content">
					<h2> Form Poin Kuesioner<br></h2>
					<p>
						<strong>Data Poin Hasil Kuesioner : </strong>
						Poin berasal dari jawaban yang telah dijawab.. Yok opo se. Yo mesti la. Wkwkwkw..
					</p>
					<div>
						<canvas id="barChart" height="140"></canvas>
					</div>
					<hr>
					<h2><b> Keterangan : </b></h2>
					@foreach($dtJawabUser as $i => $d)
						<p>
							<h3> {{($i+1).'. '.$d->soal}} </h3>
							<b>Jawab : <u>
							@if($d->jawaban == 'KEDUANYA')
								BISA YA BISA TIDAK
							@else
								{{$d->jawaban}}
							@endif
							</u></b>
						</p>
					@endforeach
				</div>
				<div class="ibox-footer">
					<span class="float-right"> The righ side of the footer </span>
					This is simple footer example
				</div>
			</div>
        </div>
    </div>
@endsection

@section('js_script')
<script src="{{asset('public/template_inspinia/js/plugins/chartJs/Chart.min.js')}}"></script>
<script src="{{asset('public/template_inspinia/js/demo/chartjs-demo.js')}}"></script>
@endsection

@section('script_tambahan')
<script>
	$(document).ready(function () {
		var barData = {
			labels: [<?php foreach($poin as $i => $p){ echo '"Soal '.($i + 1).'", '; } ?>],
			datasets: [
				/*{
					label: "Data 1",
					backgroundColor: 'rgba(220, 220, 220, 0.5)',
					pointBorderColor: "#fff",
					data: [65, 59, 80, 81, 56, 55, 40]
				},*/
				{
					label: "POIN",
					backgroundColor: 'rgba(26,179,148,0.5)',
					borderColor: "rgba(26,179,148,0.7)",
					pointBackgroundColor: "rgba(26,179,148,1)",
					pointBorderColor: "#fff",
					data: [<?php foreach($poin as $i => $p){ echo $p.', '; } ?>]
				}
			]
		};

		var barOptions = {
			responsive: true,
			scales: {
			   yAxes: [{
						display: true,
						stacked: true,
						ticks: {
							min: 0, // minimum value
							max: 100 // maximum value
						}
			   }]
			}
		};
		var ctx2 = document.getElementById("barChart").getContext("2d");
		new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});
	});
</script>
@endsection
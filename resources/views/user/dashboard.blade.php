@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('breadcrumb')
	<h2>This is main title</h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">This is</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>Breadcrumb</strong>
		</li>
	</ol>
@endsection

@section('title_action')
	<div class="title-action">
		<a href="" class="btn btn-primary">This is action area</a>
	</div>
@endsection

@section('content')
	<div class="middle-box text-center animated fadeInRightBig">
		<h3 class="font-bold">This is page content</h3>
		<div class="error-desc">
			You can create here any grid layout you want. And any variation layout you imagine:) Check out
			main dashboard and other site. It use many different layout.
			<br/><a href="index.html" class="btn btn-primary m-t">Dashboard</a>
		</div>
	</div>
@endsection
@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('breadcrumb')
	<h2><b>Add Users Page</b></h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">Laravel Pro</a>
		</li>
		<li class="breadcrumb-item">
			<a href="{{route('users.index')}}">Users</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>Add</strong>
		</li>
	</ol>
@endsection

@section('content')
	@if(session('status'))
		<div class="alert alert-success">
			{{session('status')}}
		</div>
	@endif
	<div class="row">
        <div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title bg-primary">
					<h5>## Add Data Page ##</h5>
				</div>
				<div class="ibox-content">
					<h2> Form Add Data User [<b>Only Admin</b>]<br></h2>
					<p>
						<strong>Lorem ipsum dolor</strong>
						Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
					</p>
					<form action="{{route('users.store')}}" method="POST" enctype="multipart/form-data" style="padding-top:20px;">
						@csrf
						<div class="form-group row">
							<label for="name" class="col-sm-2 col-form-label text-center"><b>Name</b></label>
							<div class="col-sm-4">
								<input class="form-control {{$errors->first('name') ? 'is-invalid' : ''}}" placeholder="Full Name" value="{{old('name')}}" type="text" name="name" id="name"/>
								<div class="invalid-feedback">
									{{$errors->first('name')}}
								</div>
							</div>
							<label for="email" class="col-sm-2 col-form-label text-center"><b>Email</b></label>
							<div class="col-sm-4">
								<input value="{{old('email')}}" class="form-control {{$errors->first('email') ? 'is-invalid' : ''}}" placeholder="user@mail.com" type="text" name="email" id="email"/>
								<div class="invalid-feedback">
									{{$errors->first('email')}}
								</div>
							</div>
						</div>
						<div class="form-group row">
							<label for="username" class="col-sm-2 col-form-label text-center"><b>Username</b></label>
							<div class="col-sm-4">
								<input value="{{old('username')}}"  class="form-control {{$errors->first('username') ? 'is-invalid' : ''}}" placeholder="username" type="text" name="username" id="username"/>
								<div class="invalid-feedback">
									{{$errors->first('username')}}
								</div>
							</div>
							<label for="access" class="col-sm-2 col-form-label text-center"><b>Access</b></label>
							<div class="col-sm-4">
								<input value="ADMIN" type="text" name="access" class="form-control" readonly>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group row">
							<div class="col-sm-6">
								<div class="row">
									<label for="phone" class="col-sm-4 col-form-label text-center"><b>Phone Number</b></label>
									<div class="col-sm-8">
										<input value="{{old('phone')}}" type="text" name="phone" class="form-control {{$errors->first('phone') ? 'is-invalid' : ''}}">
										<div class="invalid-feedback">
											{{$errors->first('phone')}}
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<label for="avatar" class="col-sm-4 col-form-label text-center"><b>Avatar Image</b></label>
									<div class="col-sm-8">
										<div class="custom-file">
											<input id="avatar" name="avatar" type="file" class="form-control custom-file-input {{$errors->first('avatar') ? 'is-invalid' : ''}}">
											<label for="avatar" class="custom-file-label">Choose file...</label>
											<div class="invalid-feedback">
												{{$errors->first('avatar')}}
											</div>
										</div>
									</div>
								</div>
							</div>
							<label for="address" class="col-sm-2 col-form-label text-center"><b>Address</b></label>
							<div class="col-sm-4">
								<textarea name="address" id="address" rows="4" class="form-control {{$errors->first('address') ? 'is-invalid' : ''}}">{{old('address')}}</textarea>
								<div class="invalid-feedback">
									{{$errors->first('address')}}
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group row">
							<label class="col-sm-2 col-form-label text-center"><b>Password</b></label>
							<div class="col-sm-4">
								<input class="form-control {{$errors->first('password') ? 'is-invalid' : ''}}" placeholder="password" type="password" name="password" id="password"/>
								<div class="invalid-feedback">
									{{$errors->first('password')}}
								</div>
							</div>
							<label class="col-sm-2 col-form-label text-center"><b>Password Confirmation</b></label>
							<div class="col-sm-4">
								<input class="form-control {{$errors->first('password_confirmation') ? 'is-invalid' : ''}}" placeholder="password confirmation" type="password" name="password_confirmation" id="password_confirmation"/>
								<div class="invalid-feedback">
									{{$errors->first('password_confirmation')}}
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group row">
							<div class="col-sm-4 col-sm-offset-2">
								<a href="{{route('users.index')}}" class="btn btn-warning btn-sm"><< Back</a>
								<button class="btn btn-primary btn-sm" name="btn_submit" type="submit">Save changes</button>
							</div>
						</div>
					</form>
				</div>
				<div class="ibox-footer">
					<span class="float-right"> The righ side of the footer </span>
					This is simple footer example
				</div>
			</div>
        </div>
    </div>
@endsection

@section('script_tambahan')
<script>
	$(document).ready(function () {
		$('.custom-file-input').on('change', function() {
			let fileName = $(this).val().split('\\').pop();
			$(this).next('.custom-file-label').addClass("selected").html(fileName);
		});
	});
</script>
@endsection
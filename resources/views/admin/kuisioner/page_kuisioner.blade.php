@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('css_script')
<link href="{{asset('public/template_inspinia/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
<link href="{{asset('public/template_inspinia/css/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
@endsection

@section('breadcrumb')
	<h2><b>Kuesioner Page</b></h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">Laravel Pro</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>Kuesioner</strong>
		</li>
	</ol>
@endsection

@section('title_action')
	<p align="right">
		<a href="{{route('kuisioner.create')}}" class="btn btn-success btn-facebook btn-outline" style="margin-top:20px;">
			<i class="fa fa-plus-circle"> </i> Add Kuesioner
		</a>
	</p>
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<b>Data Kuesioner</b>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>No</th>
									<th>Soal</th>
									<th>Poin YA</th>
									<th>Poin Tidak</th>
									<th>Tanggal</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($dtKuisioner as $index => $dk)
									<tr>
										<td>{{($index + 1)}}</td>
										<td>{{$dk->soal}}</td>
										<td>{{$dk->poin_ya}}</td>
										<td>{{$dk->poin_tidak}}</td>
										<td>{{$dk->created_at}}</td>
										<td>
											<a href="{{route('kuisioner.edit', $dk->id)}}" class="btn btn-outline btn-primary"><i class="fa fa-edit"></i> Edit</a>
											<button onclick="hapus(1)" type="button" class="btn btn-outline btn-danger"><i class="fa fa-trash"></i> Delete</button>
											<button type="button" class="btn btn-outline btn-warning" data-toggle="modal" data-target="#myModal"><i class="fa fa-search"></i> Preview</button>
										</td>
									</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>No</th>
									<th>Soal</th>
									<th>Poin YA</th>
									<th>Poin Tidak</th>
									<th>Tanggal</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content animated bounceInRight">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<i class="fa fa-laptop modal-icon"></i>
					<h4 class="modal-title">Modal title</h4>
					<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
				</div>
				<div class="modal-body">
					<p><strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
						printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,
						remaining essentially unchanged.</p>
							<div class="form-group"><label>Sample Input</label> <input type="email" placeholder="Enter your email" class="form-control"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js_script')
<script src="{{asset('public/template_inspinia/js/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('public/template_inspinia/js/plugins/dataTables/datatables.min.js')}}"></script>
<script src="{{asset('public/template_inspinia/js/plugins/dataTables/dataTables.bootstrap4.min.js')}}"></script>
@endsection

@section('script_tambahan')
<script>
	$(document).ready(function(){
		$('.dataTables-example').DataTable({
			pageLength: 25,
			responsive: true,
			dom: '<"html5buttons"B>lTfgitp',
			buttons: [
				{ extend: 'copy'},
				{extend: 'csv'},
				{extend: 'excel', title: 'ExampleFile'},
				{extend: 'pdf', title: 'ExampleFile'},

				{extend: 'print',
				 customize: function (win){
						$(win.document.body).addClass('white-bg');
						$(win.document.body).css('font-size', '10px');

						$(win.document.body).find('table')
								.addClass('compact')
								.css('font-size', 'inherit');
				}
				}
			]

		});

	});
	
    function hapus(id){
		swal({
			title: "Apakah Anda Yakin..?",
			text: "Data Anda Akan Terhapus Permanen..!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya, Hapus!",
			cancelButtonText: "Tidak, Batalkan!",
			closeOnConfirm: false,
			closeOnCancel: false 
		},
		function (isConfirm) {
			if (isConfirm) {
				$.ajax({
					url: '',
					method: 'post',
					data: {id_karyawan:id},
					dataType: 'json',
					async: false,
					success:function(data){
						if(data.status == true){
							swal({
								title: "Terhapus!", 
								text: "Data Berhasil Dihapus", 
								type: "success"
							}, function(){ 
								location.reload();
							});
						}else{
							swal({
								title: "Gagal Dihapus!", 
								text: "Telah Terjadi Kesalahan..! [001] \n Untuk Info Lebih Lanjut, Harap Hubungi Pihak IT :)", 
								type: "error"
							}, function(){ 
								location.reload();
							});
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal({
							title: "Gagal Dihapus!", 
							text: "Telah Terjadi Kesalahan..! [002] \n Untuk Info Lebih Lanjut, Harap Hubungi Pihak IT :)", 
							type: "error"
						}, function(){ 
							location.reload();
						});
					}
				});
			} else {
				swal("Dibatalkan", "Data Anda Masih Tersimpan :)", "error");
			}
		});
	}
</script>
@endsection
@extends('layouts.template_inspinia')

@section('title') Dashboard @endsection

@section('breadcrumb')
	<h2><b>Add Kuesioner Page</b></h2>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html">Laravel Pro</a>
		</li>
		<li class="breadcrumb-item">
			<a href="{{route('kuisioner.index')}}">Kuesioner</a>
		</li>
		<li class="breadcrumb-item active">
			<strong>Add</strong>
		</li>
	</ol>
@endsection

@section('content')
	@if(session('status'))
		<div class="alert alert-success">
			{{session('status')}}
		</div>
	@endif
	<div class="row">
        <div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title bg-success">
					<h5>## Add Data Page ##</h5>
				</div>
				<div class="ibox-content">
					<h2> Form Add Data Kuesioner<br></h2>
					<p>
						<strong>Tata Cara Pengisian :</strong>
						Untuk jumlah poin YA dan TIDAK totalnya tidak boleh melebihi 100 poin, kurang boleh. 
						Jadi jika poin YA bernilai 70 poin, maka poin TIDAK harus antara 0 poin dan 30 poin.
						Jika seandainya jumlah kedua poin tidak mencapai seratus, maka sisanya akan digunakan sebagai penilaian kategori poin "KEDUANYA". :)
					</p>
					<form action="{{route('kuisioner.store')}}" method="POST" enctype="multipart/form-data" style="padding-top:20px;">
						@csrf
						<div class="form-group row has-success">
							<label class="col-sm-2 col-form-label text-center"><b>Pembuat Soal</b></label>
							<div class="col-sm-10"><input value="{{Auth::user()->name.' | '.Auth::user()->email}}" type="text" name="nama" class="form-control" readonly></div>
						</div>
						<div class="form-group row has-success">
							<label class="col-sm-2 col-form-label text-center"><b>Poin [Ya]</b></label>
							<div class="col-sm-4">
								<input value="{{old('poin_ya')}}" type="number" name="poin_ya" class="form-control {{$errors->first('poin_ya') ? 'is-invalid' : ''}}" required>
								<div class="invalid-feedback">
									{{$errors->first('poin_ya')}}
								</div>
							</div>
							<label class="col-sm-2 col-form-label text-center"><b>Poin [Tidak]</b></label>
							<div class="col-sm-4">
								<input value="{{old('poin_tidak')}}" type="number" name="poin_tidak" class="form-control {{$errors->first('poin_tidak') ? 'is-invalid' : ''}}" required>
								<div class="invalid-feedback">
									{{$errors->first('poin_tidak')}}
								</div>
							</div>
						</div>
						<div class="form-group row has-success">
							<label class="col-sm-2 col-form-label text-center"><b>Soal</b></label>
							<div class="col-sm-10">
								<textarea name="soal" class="form-control m-b {{$errors->first('soal') ? 'is-invalid' : ''}}" rows="4">{{old('soal')}}</textarea>
								<div class="invalid-feedback">
									{{$errors->first('soal')}}
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group row">
							<div class="col-sm-4 col-sm-offset-2">
								<a href="{{route('kuisioner.index')}}" class="btn btn-warning btn-sm"><< Back</a>
								<button class="btn btn-primary btn-sm" name="btn_submit" type="submit">Save changes</button>
							</div>
						</div>
					</form>
				</div>
				<div class="ibox-footer">
					<span class="float-right"> The righ side of the footer </span>
					This is simple footer example
				</div>
			</div>
        </div>
    </div>
@endsection
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Register</title>
	
	<link href="{{asset('public/template_inspinia/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/template_inspinia/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('public/template_inspinia/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/template_inspinia/css/style.css')}}" rel="stylesheet">
	
</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">Register to Laravel Prototype</h2>

                <p>
                    Untuk mengganti tampilan, buka RegisterController kemudian buat function showRegistrationForm().
					Function ini akan menimpa function bawaan Auth Laravel. Jika ingin melihat daftar register function bawaan Auth Laravel, buka di : 
					<br><small><b>vendor\laravel\auth\ui\auth-backend\RegistersUsers.php</b></small>
                </p>

                <p>
                    <u>Tambahan :</u> <br> Untuk melihat daftar route Auth, bisa dilihat di => 
					<small><b> vendor/laravel/ui/src/AuthRouteMethods.php (Untuk Laravel 7)</b></small>
					<br>
					<br> Untuk Laravel 5 => <small><b> vendor\laravel\framework\src\Illuminate\Routing\Router.php </b></small>
                </p>

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" method="POST" action="{{ route('register') }}">
						@csrf
						<div class="form-group">
                            <input id="name" type="text" placeholder="Jenengmu Sopo..?" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
							@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Isekno Emailmu..."  name="email" value="{{ old('email') }}" required autocomplete="email">
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Ojo lali password mu.." class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>
						<div class="form-group">
                            <input placeholder="Tulisen passwordmu maneh.." id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
						</div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Register</button>


                        <p class="text-muted text-center">
                            <small>Already have an account?</small>
                        </p>
                        <a href="{{ url('/login') }}" class="btn btn-sm btn-white btn-block">Login Here</a>
                    </form>
                    <p class="m-t">
                        <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div>
    </div>

</body>

</html>
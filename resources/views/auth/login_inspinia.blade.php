<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login 2</title>
	
	<link href="{{asset('public/template_inspinia/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/template_inspinia/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('public/template_inspinia/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/template_inspinia/css/style.css')}}" rel="stylesheet">
	
</head>

<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                <h2 class="font-bold">Welcome to Laravel Prototype</h2>

                <p>
                    Halaman ini menggunakan Auth bawaan Laravel. Percobaan dimulai dengan mengubah tampilan form login bawaan laravel.
                </p>

                <p>
                    Biar ndak lupa, sintaks membuat auth laravel (sintaks ini untuk laravel versi 6 keatas, jika dibawah 6 beda lagi. Palengg..) : 
                </p>
				
				<ol>
					<li> composer require laravel/ui </li>
					<li> php artisan ui vue --auth </li>
					<li> npm install && npm run dev </li>
				</ol>

                <p>
                    Untuk mengganti tampilan, buka LoginController kemudian buat function showLoginForm().
					Function ini akan menimpa function bawaan Auth Laravel. Jika ingin melihat daftar function bawaan Auth Laravel, buka di : 
                </p>

                <p>
                    <small><b>vendor\laravel\auth\ui\auth-backend\AuthenticatesUsers.php</b></small>
                </p>

            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
						@csrf
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Isekno Email mu..."  name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
							@error('email')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>
                        <div class="form-group">
                            <input type="password" placeholder="Ojo lali password mu yoan.." class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
							@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
                        </div>
						<div class="form-group">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

								<label class="form-check-label" for="remember">
									{{ __('Remember Me') }}
								</label>
							</div>
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

						@if (Route::has('password.request'))
							<a href="{{ route('password.request') }}">
								<small> {{ __('Forgot Your Password?') }} </small>
							</a>
						@endif

                        <p class="text-muted text-center">
                            <small>Do not have an account?</small>
                        </p>
                        <a href="{{ url('/register') }}" class="btn btn-sm btn-white btn-block">Create an account</a>
                    </form>
                    <p class="m-t">
                        <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div>
    </div>

</body>

</html>
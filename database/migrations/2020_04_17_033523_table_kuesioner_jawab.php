<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableKuesionerJawab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuesioner_jawab', function (Blueprint $table) {
            $table->increments('id');
            $table->text("soal_id");
			$table->enum("jawaban", ["YA", "TIDAK", "KEDUANYA"]);
			$table->integer("penjawab");
			$table->integer("poin")->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kuesioner_jawab');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnKuesioner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		//cek apakah sudah ada tablenya
		if (Schema::hasTable('kuesioner_jawab')) {
			//cek apakah kolom tabelnya ada
			if (Schema::hasColumn('kuesioner_jawab', 'jawaban')) {
				//baru eksekusi (khusus untuk updete kolom tipe ENUM, harus install dulu "dbal")
				//composer require doctrine/dbal
				Schema::table('kuesioner_jawab', function (Blueprint $table) {
					$table->enum("jawaban", ["YA", "TIDAK", "KEDUANYA"])->change();
				});
			}
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //cek apakah sudah ada tablenya
		if (Schema::hasTable('kuesioner_jawab')) {
			//cek apakah kolom tabelnya ada
			if (Schema::hasColumn('kuesioner_jawab', 'jawaban')) {
				//baru eksekusi
				Schema::table('kuesioner_jawab', function (Blueprint $table) {
					$table->enum("jawaban", ["YA", "TIDAK"])->change();
				});
			}
		}
    }
}

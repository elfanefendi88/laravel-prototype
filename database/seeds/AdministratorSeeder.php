<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\User;
		$administrator->username = "elfanefendi88";
		$administrator->name = "Muhammad Elfan Efendi";
		$administrator->email = "elfanefendi88@gmail.com";
		$administrator->roles = json_encode(["ADMIN"]);
		$administrator->password = \Hash::make("password");
		$administrator->avatar = "saat-ini-tidak-ada-file.png";
		$administrator->address = "Jombang City";
		$administrator->save();
		$this->command->info("User Admin berhasil diinsert");
    }
}

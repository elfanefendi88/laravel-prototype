<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$users = \App\User::all();
        return view('admin.user.page_user', ['aktif_user' => 'class=active', 'dtUser' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.add_user', ['aktif_user' => 'class=active']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		\Validator::make($request->all(),[
			"name" => "required|min:5|max:100",
			"username" => "required|min:5|max:20",
			"phone" => "digits_between:10,12",
			"address" => "min:20|max:200",
			"email" => "required|email|unique:App\User,email",
			"password" => "required",
			"password_confirmation" => "required|same:password"
		])->validate();
		
        $new_user = new \App\User;
		
		$new_user->name = $request->get('name');
		$new_user->username = $request->get('username');
		$new_user->roles = json_encode(array('ADMIN'));
		$new_user->name = $request->get('name');
		$new_user->address = $request->get('address');
		$new_user->phone = $request->get('phone');
		$new_user->email = $request->get('email');
		$new_user->password = \Hash::make($request->get('password'));
		
		if($request->file('avatar')){
			$file = $request->file('avatar')->store('avatars', 'public');
			$new_user->avatar = $file;
		}
		
		$new_user->save();
		
		return redirect()->route('users.create')->with('status', 'User successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$data['user'] = \App\User::findOrFail($id);
		$data['aktif_user'] = 'class=active';
        return view('admin.user.edit_user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        \Validator::make($request->all(), [
			"name" => "required|min:5|max:100",
			"username" => "required|min:5|max:20",
			"phone" => "digits_between:10,12",
			"address" => "min:20|max:200",
		])->validate();
		
        $user = \App\User::findOrFail($id);
		$user->name = $request->get('name');
		$user->address = $request->get('address');
		$user->phone = $request->get('phone');
		$user->username = $request->get('username');
		
		if($request->file('avatar')){
			if($user->avatar && file_exists(storage_path('app/public/' . $user->avatar))){
				\Storage::delete('public/'.$user->avatar);
			}
			$file = $request->file('avatar')->store('avatars', 'public');
			$user->avatar = $file;
		}
		$user->save();
		return redirect()->route('users.edit', [$id])->with('status', 'User succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

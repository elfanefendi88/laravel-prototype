<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;				//untuk percobaan Query Builder

class KuisionerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data['dtKuisioner'] = \App\Kuisioner::where('pembuat', \Auth::user()->id)->get();
		//$data['dtKuisioner'] = \App\Kuisioner::all();
		$data['aktif_kuisioner'] = 'class=active';
        return view('admin.kuisioner.page_kuisioner', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.kuisioner.add_kuisioner', ['aktif_kuisioner' => 'class=active']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if($request->get('poin_ya') > 100 or $request->get('poin_tidak') > 100){
			$rules['poin_tidak'] = 'required|numeric|min:0|max:100';
			$rules['poin_ya'] = 'required|numeric|min:0|max:100';
		}else{
			if($request->get('poin_ya') > $request->get('poin_tidak')){
				$rules['poin_tidak'] = 'required|numeric|min:0|max:'.(100 - $request->get('poin_ya'));
				$rules['poin_ya'] = 'required|numeric|min:0|max:100';
			}else{
				$rules['poin_tidak'] = 'required|numeric|min:0|max:100';
				$rules['poin_ya'] = 'required|numeric|min:0|max:'.(100 - $request->get('poin_tidak'));
			}
		}
		$rules['soal'] = 'required';
		
        \Validator::make($request->all(), $rules)->validate();
		
		$new_Kuisioner = new \App\Kuisioner;
		$new_Kuisioner->soal = $request->get('soal');
		$new_Kuisioner->poin_ya = $request->get('poin_ya');
		$new_Kuisioner->poin_tidak = $request->get('poin_tidak');
		$new_Kuisioner->pembuat = \Auth::user()->id;
		$new_Kuisioner->save();
		
		return redirect()->route('kuisioner.create')->with('status', 'Kuesioner Berhasil Dibuat..');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kuis'] = \App\Kuisioner::findOrFail($id);
		$data['aktif_kuisioner'] = 'class=active';
        return view('admin.kuisioner.edit_kuisioner', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		
		if($request->get('poin_ya') > 100 or $request->get('poin_tidak') > 100){
			$rules['poin_tidak'] = 'required|numeric|min:0|max:100';
			$rules['poin_ya'] = 'required|numeric|min:0|max:100';
		}else{
			if($request->get('poin_ya') > $request->get('poin_tidak')){
				$rules['poin_tidak'] = 'required|numeric|min:0|max:'.(100 - $request->get('poin_ya'));
				$rules['poin_ya'] = 'required|numeric|min:0|max:100';
			}else{
				$rules['poin_tidak'] = 'required|numeric|min:0|max:100';
				$rules['poin_ya'] = 'required|numeric|min:0|max:'.(100 - $request->get('poin_tidak'));
			}
		}
		$rules['soal'] = 'required';
		
        \Validator::make($request->all(), $rules)->validate();
		
        $kuis = \App\Kuisioner::findOrFail($id);
		$kuis->poin_ya = $request->get('poin_ya');
		$kuis->poin_tidak = $request->get('poin_tidak');
		$kuis->soal = $request->get('soal');
		
		$kuis->save();
		return redirect()->route('kuisioner.edit', [$id])->with('status', 'Soal Kuesioner Berhasil Diubah.. Yeahhh');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function listmaker()
    {
		//untuk select data di function ini menggunakan "Query Builder".
		//syarat untuk menggunakan query builder adalah harus mennggil class tertentu, yaitu
		//"use Illuminate\Support\Facades\DB;" , kalau pakek "Eloquent" kita tidak usah menggil class itu, cukup buat model saja..
		
		//query dibawah akan error, tp jika di jalankan lewat mysql phpmyadmin tidak akan error. ndak tau kenapa
		//solusi biar ndak error, mengganti settingan database di "config\database.php"
		//kemudian cari "strict" dan ubah nilainya menjadi false
		$data['dtKuisioner'] = DB::table('users')
			->join('kuesioner_soal', 'kuesioner_soal.pembuat', '=', 'users.id')
			->select('users.*', DB::raw("count(kuesioner_soal.id) as jum_kuis"))
			->groupBy('users.id')
			->orderByRaw('users.id DESC')
			->get();
		$data['dtJawabUser'] = DB::table('kuesioner_soal')
			->join('kuesioner_jawab', 'kuesioner_jawab.soal_id', '=', 'kuesioner_soal.id')
			->select('kuesioner_soal.pembuat', DB::raw("count(kuesioner_jawab.id) as jum_jawab"))
			->where('kuesioner_jawab.penjawab', '=', \Auth::user()->id)
			->groupBy('kuesioner_soal.pembuat')
			->orderByRaw('kuesioner_soal.pembuat DESC')
			->get();
		$data['aktif_kuisuser'] = 'class=active';
        return view('user.kuisioner.page_listmaker', $data);
    }
	
	public function start($id)
    {
		$data['pembuat'] = \App\User::findOrFail($id);
		$data['soal'] = DB::table('kuesioner_soal')
			->select('kuesioner_soal.id', 'kuesioner_soal.soal', 'kuesioner_jawab.jawaban', 'kuesioner_jawab.created_at', 'kuesioner_jawab.updated_at')
            ->leftJoin('kuesioner_jawab', 'kuesioner_soal.id', '=', 'kuesioner_jawab.soal_id')
			->where('kuesioner_soal.pembuat', '=', $id)
			->orderBy('kuesioner_soal.created_at')
            ->paginate(2);
		$data['cek'] = DB::table('kuesioner_soal')
            ->join('kuesioner_jawab', 'kuesioner_jawab.soal_id', '=', 'kuesioner_soal.id')
			->where('kuesioner_soal.pembuat', '=', $id)
            ->select('kuesioner_soal.*')
            ->get();
		$data['aktif_kuisuser'] = 'class=active';
        return view('user.kuisioner.start_kuisioner', $data);
    }
	
	public function simpan_kuesioner(Request $request)
    {
		//$status = array();
		
		for($i=0;$i<$request->get('jumlah');$i++){
			/*
			$dtCek = DB::table('kuesioner_jawab')->where([
				['soal_id', '=', $request->get('identity'.$i)],
				['penjawab', '=', \Auth::user()->id],
			])->get();
			
			if(count($dtCek) > 0){
				$status[$i] = 'update';
			}else{
				$status[$i] = 'create';
			}
			
			$dataInput[$i] = array(
				'created_at' => \Carbon\Carbon::now(),
				'soal_id' => $request->get('identity'.$i),
				'penjawab' => \Auth::user()->id,
				'jawaban' => $request->get('jawab'.$i),
			);
			*/
			
			//jika suah ada datanya, maka update. jika belum maka insert
			//parameter pertama untuk cek kondisi update
			//parameter ke2 untuk yang diupdate atau jika insert, maka parameter 1 dan 2 akan dimerge
			DB::table('kuesioner_jawab')->updateOrInsert(
				['soal_id' => $request->get('identity'.$i), 'penjawab' => \Auth::user()->id],
				['created_at' => \Carbon\Carbon::now(), 'jawaban' => $request->get('jawab'.$i)]
			);
		}
		
		/*
		if(in_array('update', $status)){
			if(in_array('create', $status)){
				//perulangan dtinput
			}else{
				$affected = DB::table('users')
					->where('id', 1)
					->update(['votes' => 1]);
			}
		}else{
			DB::table('kuesioner_jawab')->insert($dataInput);
		}
		*/
		
		return redirect()->back()->with('message','Operation Successful !');
    }
	
	public function show_poin($id)
    {
		$poin = array();
		
		$data['pembuat'] = \App\User::findOrFail($id);
		$dtJawabUser = DB::table('kuesioner_soal')
			->join('kuesioner_jawab', 'kuesioner_jawab.soal_id', '=', 'kuesioner_soal.id')
			->select('kuesioner_soal.*', 'kuesioner_jawab.jawaban as jawaban')
			->where([
				['kuesioner_jawab.penjawab', '=', \Auth::user()->id],
				['kuesioner_soal.pembuat', '=', $id],
			])
			->orderBy('kuesioner_soal.created_at')
			->get();
		
		foreach($dtJawabUser as $dju){
			if($dju->jawaban == 'YA'){
				$poin[] = $dju->poin_ya;
			}else if($dju->jawaban == 'TIDAK'){
				$poin[] = $dju->poin_tidak;
			}else{
				if($dju->poin_tidak > $dju->poin_ya){
					$poin[] = $dju->poin_tidak - $dju->poin_ya;
				}else{
					$poin[] = $dju->poin_ya - $dju->poin_tidak;
				}
			}
		}
		
		$data['poin'] = $poin;
		$data['dtJawabUser'] = $dtJawabUser;
		
		$data['aktif_kuisuser'] = 'class=active';
        return view('user.kuisioner.poin_kuisioner', $data);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class AksesAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		//ini mengecek auth + akses admin, jadi nanti di route tidak usah pakek middleware auth. soalnya sudah ada function "auth()->check()"
		if(auth()->check() && in_array("ADMIN", json_decode($request->user()->roles))){
			return $next($request);
		}
		//return redirect('/home');
		return back()->withInput();					//ini digunakan untuk redirect halaman sebelumnya
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class AksesUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && in_array("USER", json_decode($request->user()->roles))){
			return $next($request);
		}
		return redirect('/home');
    }
}

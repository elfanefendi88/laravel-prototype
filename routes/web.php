<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//========== halaman depan atau front end ================
Route::get('/', function () {
    return view('welcome');
});

//========= akses yang berhubungan authentikasi ==========
Auth::routes();

//======= menu user ============
Route::resource("users", "UserController")->middleware('aksesadmin');

//====== menu dashboard ==========
Route::get('/home', 'HomeController@index')->name('home');

//======= menu kuisioner ============
Route::get('/listmaker', 'KuisionerController@listmaker')->middleware('aksesuser');
Route::get('/kuisioner/start/{id}', 'KuisionerController@start')->middleware('aksesuser');
Route::get('/kuisioner/poin/{id}', 'KuisionerController@show_poin')->middleware('aksesuser');
Route::post('/simpan_kuesioner', 'KuisionerController@simpan_kuesioner')->middleware('aksesuser');
Route::resource("kuisioner", "KuisionerController")->middleware('aksesadmin');